//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package read_me_generator;

import org.tango.pogo.pogo_gui.tools.PogoException;
import org.tango.pogo.pogo_gui.tools.Utils;

import javax.swing.*;
import java.io.File;

import static read_me_generator.GenerateInfoFiles.READ_ME_FILE;

/**
 * This class is able to parse project info to build a README.md file
 *
 * @author verdier
 */

public class ReadMeGenerator {
    private static final boolean generate = true;
    //===============================================================
    //===============================================================
    public ReadMeGenerator(String fileName) throws PogoException {
        TangoProject project = new TangoProject(fileName);
        System.out.println(project);

        //  Get validation for description
        ProjectDescriptionDialog descriptionDialog =
                new ProjectDescriptionDialog(new JFrame(), project);
        if (descriptionDialog.showDialog()==JOptionPane.CANCEL_OPTION) {
            System.exit(0);
        }

        if (new File(READ_ME_FILE).exists()) {
            if (JOptionPane.showConfirmDialog(new JFrame(),
                    " file already exists. Overwrite it ?",
                    "confirmation",
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE)==JOptionPane.CANCEL_OPTION) {
                return;
            }
            else
            if (!new File(READ_ME_FILE).delete()) {
                System.err.println("Cannot remove " + READ_ME_FILE);
                return;
            }
        }

        if (generate) {
            //  create files (if linux)
            if (Utils.osIsUnix())
                executeCreationScript();
            new GenerateInfoFiles(project);
        }
    }
    //===============================================================
    //===============================================================
    private void executeCreationScript() throws PogoException {
        String scriptName = System.getenv("SCRIPT_NAME");
        if (scriptName==null) {
            throw  new PogoException("Environment SCRIPT_NAME not set !");
        }
        if (!new File(scriptName).exists()) {
            throw  new PogoException(scriptName + " not found !");
        }
        scriptName += " cpp --install";
        System.out.println("Executing " + scriptName);
        Utils.executeShellCommand(scriptName);
    }
    //===============================================================
    //===============================================================
    public static void main(String[] args) {

        String fileName = null;//"Y:/tango/cppserver/admin/starter/Starter.xmi";
        if (args.length>0)
            fileName = args[0];
        try {
            new ReadMeGenerator(fileName);
         } catch (PogoException e) {
            //e.printStackTrace();
            System.err.println("\n\n"+e.toString());
        }
        System.exit(0);
    }
    //===============================================================
    //===============================================================
}
