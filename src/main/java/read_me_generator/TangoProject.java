//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package read_me_generator;

import fr.esrf.tango.pogo.pogoDsl.PogoMultiClasses;
import org.tango.pogo.pogo_gui.DeviceClass;
import org.tango.pogo.pogo_gui.tools.OAWutils;
import org.tango.pogo.pogo_gui.tools.ParserTool;
import org.tango.pogo.pogo_gui.tools.PogoException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is able to parse different files (pom, xmi, ...)
 * and models a C++ or Java project.
 *
 * @author verdier
 */

public class TangoProject {
    private String projectName;
    private String url;
    private String projectDescription;
    private String language = "Cpp";
    private MavenProject mavenProject;
    private String repositoryName = "Git repository not found";
    private int repositoryType;
    private String license = "";
    private static final int GIT_UB = 0;
    private static final int GIT_LAB = 1;
    private static final String[] REPOSITORY_NAMES = { "github", "gitlab" };
    private static final String  debugPath = System.getenv("DebugPath");
    //===============================================================
    //===============================================================
    public TangoProject(String fileName) throws PogoException {
        checkGitRepository(fileName);
        mavenProject = new MavenProject();
        if (mavenProject.isMavenProject())
            language = "Java";
        if (fileName!=null) {
            File file = new File(fileName);
            if (file.exists()) {
                loadProjectDescription(fileName);
            }
            else
                System.err.println(fileName + " NOT found");
        }
        else
        fileName = checkXmiFiles();
        if (fileName!=null) {
            loadProjectDescription(fileName);
        }
        else
        if (new File("README").exists())
            projectDescription = ParserTool.readFile("README");
    }
    //===============================================================
    //===============================================================
    private String checkXmiFiles() {
        //  Search in project structure (maven ,...)
        String xmiFile = searchInPath(".", ".multi.xmi");
        if (xmiFile==null)
            xmiFile = searchInPath(".", ".xmi");
        return xmiFile;
    }
    //===============================================================
    //===============================================================
    private String searchInPath(String path, String target) {
        //System.out.println("search in " + path);
        File dir = new File(path);
        String[] fileNames = dir.list();
        if (fileNames!=null) {
            for (String fileName : fileNames) {
                // Do not search in .git, .idea, maven bin, ...
                if (!fileName.startsWith(".") && !fileName.contains("target")) {
                    String absoluteName = path + '/' + fileName;
                    File file = new File(absoluteName);
                    if (file.isDirectory()) {
                        //  Search in sub dir
                        String xmiFile = searchInPath(absoluteName, target);
                        if (xmiFile != null)
                            return xmiFile;
                    } else {
                        //  Check the language
                        if (fileName.endsWith(".cpp")) language = "Cpp";
                        else if (fileName.endsWith(".java")) language = "Java";
                        else if (fileName.endsWith(".py")) language = "Python";
                        if (fileName.endsWith(target))
                            return absoluteName;
                    }
                }
            }
        }
        return null;
    }
    //===============================================================
    //===============================================================
    public void setProjectDescription(String description) {
        this.projectDescription = description;
    }
    //===============================================================
    //===============================================================
    private void loadProjectDescription(String fileName) throws PogoException {
        if (fileName.endsWith(".multi.xmi")) {
            PogoMultiClasses multiClasses = OAWutils.getInstance().loadMultiClassesModel(fileName);
            projectDescription = multiClasses.getDescription();
            license = multiClasses.getLicense();
        }
        else
        if (fileName.endsWith(".xmi")) {
            DeviceClass deviceClass = new DeviceClass(fileName);
            projectDescription = deviceClass.getPogoDeviceClass().getDescription().getDescription();
            language = deviceClass.getPogoDeviceClass().getDescription().getLanguage();
            license = deviceClass.getPogoDeviceClass().getDescription().getLicense();
        }
        else {
            if (fileName.endsWith("README")) {
                projectDescription = ParserTool.readFile(fileName);
            }
        }
    }
    //===============================================================
    //===============================================================
    private void checkGitRepository(String fileName) throws PogoException {
        String target = "url = ";
        String projectPath;
        if (fileName==null)
            projectPath = (debugPath==null)? "." : debugPath;
        else
            projectPath = getPath(fileName);
        List<String> lines = ParserTool.readFileLines(projectPath+"/.git/config", false);
        for (String line : lines)
            if (line.startsWith(target))
                repositoryName = line.substring(target.length()).trim();
        repositoryType = getRepositoryType(repositoryName);
        projectName = parseProjectName();
        switch (repositoryType) {
            case GIT_UB:
                url = repositoryName;
                buildGitHubRepositoryName();
                break;
            case GIT_LAB:
                buildGitLabUrlName();
                break;
        }
    }
    //===============================================================
    //===============================================================
    private void buildGitLabUrlName() {
        String target = "gitlab.esrf.fr";
        int idx = repositoryName.indexOf(target);
        if (idx>0) {
            url = "https://" + target + "/A" + repositoryName.substring(idx+target.length()+":a".length());
        }
    }
    //===============================================================
    //===============================================================
    private void buildGitHubRepositoryName() {
        String target = "github.com/";
        int idx = repositoryName.indexOf(target);
        if (idx>0)
            repositoryName = "git@github.com:" + repositoryName.substring(idx+target.length());
    }
    //===============================================================
    //===============================================================
    private String parseProjectName() {
        if (repositoryName.endsWith(".git"))
            repositoryName = repositoryName.substring(0, repositoryName.length()-4);
        String path = getPath(repositoryName);
        return repositoryName.substring(path.length()+1);
    }
    //===============================================================
    //===============================================================
    private int getRepositoryType(String repository) throws PogoException {
        int i=0;
        for (String repositoryName : REPOSITORY_NAMES) {
            if (repository.contains(repositoryName)) {
                return i;
            }
            i++;
        }
        throw new PogoException("UNKNOWN REPOSITORY");
    }
    //===============================================================
    //===============================================================
    private static String getPath(String filename) {
        int pos = filename.lastIndexOf('/');
        int pos1 = filename.lastIndexOf('\\');
        String path = "./";
        if (pos > 0) {
            path = filename.substring(0, pos);
        }
        else
        if (pos1 > 0) {
            path = filename.substring(0, pos1);
        }
        return path;
    }
    //===============================================================
    //===============================================================


    //===============================================================
    //===============================================================
    public String getProjectName() {
        return projectName;
    }
    //===============================================================
    //===============================================================
    public String getUrl() {
        return url;
    }
    //===============================================================
    //===============================================================
    public String getProjectDescription() {
        return projectDescription;
    }
    //===============================================================
    //===============================================================
    public String getLanguage() {
        return language;
    }
    //===============================================================
    //===============================================================
    public String getRepositoryName() {
        return repositoryName;
    }
    //===============================================================
    //===============================================================
    public boolean isMavenProject() {
        return mavenProject.isMavenProject();
    }
    //===============================================================
    //===============================================================
    public String getLicense() {
        return license;
    }
    //===============================================================
    //===============================================================
    public void setJarDependencies(List<String> list) {
        if (language.toLowerCase().equals("java"))
            mavenProject.setJarDependencies(list);
    }
    //===============================================================
    //===============================================================
    public List<String> getJarDependencies() {
        if (language.toLowerCase().equals("java"))
            return mavenProject.getDependencies();
        else
            return new ArrayList<>();
    }
    //===============================================================
    //===============================================================
    public String toString() {
        StringBuilder str = new StringBuilder(
                "Project Name:     " + projectName + '\n' +
                "Repository type:  " + REPOSITORY_NAMES[repositoryType] + '\n' +
                "Repository name:  " + repositoryName + '\n' +
                "URL:              " + url + '\n' +
                "language:         " + language + '\n' +
                "Licence:          " + license);
        if (mavenProject.isMavenProject()) {
            str.append(mavenProject);
        }

        if (projectDescription !=null)
            str.append("\n\nClass Description:\n").append(projectDescription);
        return str.toString();
    }
    //===============================================================
    //===============================================================
}
