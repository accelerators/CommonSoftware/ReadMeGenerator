//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package read_me_generator;

import org.tango.pogo.pogo_gui.tools.ParserTool;
import org.tango.pogo.pogo_gui.tools.PogoException;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class is able to check if it is a Maven project and parse dependencies
 *
 * @author verdier
 */

public class MavenProject {
    private List<String> dependencies = new ArrayList<>();
    private boolean mavenProject;
    private static final String[] DEFAULT_JAR_FILES = {
            "JTango.jar", "ATKCore.jar", "ATKWidget.jar", "atkpanel.jar",
    };
    //===============================================================
    //===============================================================
    MavenProject() {
        String fileName = "./pom.xml";
        if (new File(fileName).exists() && mavenArchitectureAvailable()) {
            mavenProject = true;
            try {
                Collections.addAll(dependencies, DEFAULT_JAR_FILES);
                List<String> lines = ParserTool.readFileLines(fileName, false);
                //  Search begin/end of dependencies
                boolean in = false;
                for (String line : lines) {
                    if (line.contains("<dependencies>")) in = true;
                    else if (line.contains("</dependencies>")) in = false;
                    else if (in) {
                        int idx = line.indexOf("<artifactId>");
                        if (idx >= 0) {
                            idx += "<artifactId>".length();
                            String dependency =
                                    line.substring(idx, line.indexOf("</")).trim()+".jar";
                            //  Add it if not already done
                            if (!dependencyAlreadyKnown(dependency))
                                dependencies.add(0, dependency);
                        }
                    }
                }
            } catch (PogoException e) {
                System.err.println(e.toString());
            }
        }
        else
            mavenProject = false;
    }
    //===============================================================
    //===============================================================
    private boolean mavenArchitectureAvailable() {
        return new File("src/main/java").exists();
    }
    //===============================================================
    //===============================================================
    private boolean dependencyAlreadyKnown(String dependency) {
        for (String str : dependencies)
            if (dependency.equals(str))
                return true;
        return false;
    }
    //===============================================================
    //===============================================================
    public void setJarDependencies(List<String> list) {
        dependencies = list;
    }
    //===============================================================
    //===============================================================
    public List<String> getDependencies() {
        return dependencies;
    }
    //===============================================================
    //===============================================================
    public boolean isMavenProject() {
        return mavenProject;
    }
    //===============================================================
    //===============================================================
    public String toString() {
        StringBuilder str = new StringBuilder(mavenProject? "\nDependencies:\n":"");
            for (String dependency : dependencies)
                str.append("    - ").append(dependency).append("\n");
        return str.toString();
    }
    //===============================================================
    //===============================================================
}
