//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package read_me_generator;

import org.tango.pogo.pogo_gui.tools.ParserTool;
import org.tango.pogo.pogo_gui.tools.PogoException;

import java.io.File;


/**
 * This class is able to generate README.md and INSTALL.md files from a project object
 *
 * @author verdier
 */

public class GenerateInfoFiles {
    private TangoProject project;
    private String templateCode;

    static final String READ_ME_FILE = "README.md";
    static final String INSTALL_FILE = "INSTALL.md";
    //===============================================================
    //===============================================================
    public GenerateInfoFiles(TangoProject project) throws PogoException {
        this.project = project;
        generateReadMeFile();
        generateInstallFile();
    }
    //===============================================================
    //===============================================================
    private void generateReadMeFile() throws PogoException {
        templateCode = ParserTool.readFile(READ_ME_FILE);
        setProjectName();
        setCloningInfo();
        setDocumentation();
        setDependencies();
        setToolChain();
        setBuild();
        //setLicense();
        cleanReadMeFile();

        ParserTool.writeFile(READ_ME_FILE, templateCode);
        System.out.println(READ_ME_FILE + " generated");
    }
    //===============================================================
    //===============================================================
    private void generateInstallFile() throws PogoException {
        /*
        templateCode = ParserTool.readFile(INSTALL_FILE);
        setDependencies();
        setToolChain();
        setBuild();
        setRunTimeParameters();

        ParserTool.writeFile(INSTALL_FILE, templateCode);
        System.out.println(INSTALL_FILE + " generated");
        */
        File install = new File(INSTALL_FILE);
        if (install.exists())
            if (!install.delete())
                System.err.println("Cannot remove " + INSTALL_FILE);
    }
    //===============================================================
    //===============================================================
    private void setProjectName() {
        String target = "# Project ";
        int idx = templateCode.indexOf(target);
        if (idx>=0) {
            String code = project.getProjectName() + "\n\n" +
                            (project.isMavenProject()? "Maven " : "") +
                            project.getLanguage() +  " project\n\n";
            if (project.getProjectDescription().isEmpty())
                code += "A short summary of what the project is and does.";
            else
                code += project.getProjectDescription() + "\n";

            templateCode = templateCode.substring(0, idx+target.length()) +
                    code + getCodeAfterTarget("## ", idx);
        }
    }
    //===============================================================
    //===============================================================
    private void setCloningInfo() {
        String target = "## Cloning";
        int idx = templateCode.indexOf(target);
        if (idx>=0) {
            int idx2 = templateCode.indexOf("`");
            if (idx2<0) idx2 = idx;
            templateCode = templateCode.substring(0, idx) + "\n\n" +
                    target + "\n\n" + templateCode.substring(idx2);

            idx = templateCode.indexOf("git@", idx);
            if (idx>0) {
                templateCode = templateCode.substring(0, idx) +
                    project.getRepositoryName() +
                    getCodeAfterTarget("\n", idx);
            }
        }
    }
    //===============================================================
    //===============================================================
    private void setDocumentation() {
        String target = "## Documentation";
        int idx = templateCode.indexOf(target);
        if (idx>=0) {
            idx = templateCode.indexOf("\n", idx);
            if (idx>0 && !templateCode.contains("See [Wiki]")) {
                idx++;
                templateCode = templateCode.substring(0, idx) +
                    "\nSee [Wiki](" + project.getUrl() + "/wikis/home) for more details.\n" +
                    getCodeAfterTarget("\n", idx);
            }
        }
    }
    //===============================================================
    //===============================================================
    private void setDependencies() {
        if (project.getLanguage().toLowerCase().equals("java")) {
            String target = "## Project Dependencies";
            int idx = templateCode.indexOf(target);
            if (idx >= 0) {
                idx = templateCode.indexOf("\n", idx);
                if (idx>0) {
                    idx++;
                    StringBuilder code = new StringBuilder("\n");
                    for (String dependency : project.getJarDependencies())
                        code.append("* ").append(dependency).append('\n');

                    templateCode = templateCode.substring(0, idx) +
                            code + getCodeAfterTarget("* Any other dependencies", idx);
                }
            }
        }
    }
    //===============================================================
    //===============================================================
    private void setToolChain() {
        if (project.getLanguage().toLowerCase().equals("java")) {
            String target = "## Toolchain Dependencies";
            int idx = templateCode.indexOf(target);
            if (idx >= 0) {
                idx = templateCode.indexOf("\n", idx);
                if (idx>0) {
                    idx++;
                    StringBuilder code = new StringBuilder("\n");
                    code.append("* javac 7 or higher\n");
                    if (project.isMavenProject())
                        code.append("* maven\n");

                    templateCode = templateCode.substring(0, idx) +
                            code + getCodeAfterTarget("* Any other dependencies", idx);
                }
            }
        }
    }
    //===============================================================
    //===============================================================
    private void setBuild() {
        if (project.getLanguage().toLowerCase().equals("java")) {
            String target = "## Build\n";
            int idx = templateCode.indexOf(target);
            if (idx >= 0) {
                String code = "\n\nInstructions on building the project.\n\n";
                code += "```\ncd " + project.getProjectName() + "\n";
                if (project.isMavenProject()) {
                    code += "mvn package\n";
                }
                else {
                    code += "make\n";
                }
                code += "```\n\n";
                templateCode = templateCode.substring(0, idx+target.length()) +
                            code + getCodeAfterTarget("## Installation", idx);
            }
        }
    }
    //===============================================================
    //===============================================================
    private void setLicense() {
        String license = project.getLicense();
        if (license!=null && !license.isEmpty()) {
            String target = "## License";
            int idx = templateCode.indexOf(target);
            if (idx >= 0) {
                idx = templateCode.indexOf("<GPL", idx);
                if (idx>0) {
                    templateCode = templateCode.substring(0, idx) +
                            "** " + license + "3 ** license\n";
                }
            }
        }
    }
    //===============================================================
    //===============================================================
    private void cleanReadMeFile() {
        cleanReadMeFile("**(Delete If Does Not Apply)**");
        cleanReadMeFile("* Any other dependencies");
        cleanReadMeFile(" --recurse-submodules");

        int end = templateCode.indexOf("## Installation");
        if (end>0)
            templateCode = templateCode.substring(0, end);
    }
    //===============================================================
    //===============================================================
    private void cleanReadMeFile(String target) {
        int start;
        int end = 0;
        StringBuilder sb = new StringBuilder();
        while ((start=templateCode.indexOf(target, end))>0) {
            sb.append(templateCode.substring(end, start));
            end = start+target.length();
        }
        sb.append(templateCode.substring(end));
        templateCode = sb.toString();
    }
    //===============================================================
    //===============================================================
    private void setRunTimeParameters() {
        if (project.getLanguage().toLowerCase().equals("java")) {
            String target = "## Installation";
            int idx = templateCode.indexOf(target);
            if (idx > 0) {
                idx = templateCode.indexOf("?", idx);
                if (idx>0) {
                    idx = templateCode.indexOf("\n", idx);
                    StringBuilder code;
                    if (idx>0)
                         code = new StringBuilder(templateCode.substring(0, idx));
                    else
                         code = new StringBuilder(templateCode);

                    //  Add example to run it
                    code.append("\n\n```\n");
                    code.append("JARS=<path/to/jar/files>\n");
                    code.append("CLASSPATH=");
                    for (String dependency : project.getJarDependencies())
                        code.append("$JAR/").append(dependency).append(":");

                    //  Remove last ':' and add final code
                    templateCode = code.toString();
                    templateCode = templateCode.substring(0, templateCode.length() - 1) +
                            "\nexport CLASSPATH\n" +
                            "java  <package>.<main class>\n" +
                            "```\n\n";
                }
            }
        }
    }
    //===============================================================
    //===============================================================
    private String getCodeAfterTarget(String target,  int index) {
        return templateCode.substring(templateCode.indexOf(target, index));
    }
    //===============================================================
    //===============================================================
}
