//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package read_me_generator.tools;

import org.tango.pogo.pogo_gui.tools.ParserTool;
import org.tango.pogo.pogo_gui.tools.PogoException;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This class is able to update release number with date
 *      and generate html ReleaseNote.java
 *
 * @author verdier
 */

public class UpdateJavaReleaseNotes {
    private String notesPath;
    private static final String CHANGE_LOG_FILE = "CHANGELOG.md";
    private static final String RELEASE_CLASS = "ReleaseNotes";
    private static final String RELEASE_FILE = RELEASE_CLASS + ".java";
    //===============================================================
    //===============================================================
    public UpdateJavaReleaseNotes(String notesPath) throws PogoException {
        this.notesPath = notesPath;
        String packageName = getPackageName() ;
        System.out.println("Will generate " + notesPath + '/' + RELEASE_FILE + "\n with package " + packageName);

        //  Parse CHANGELOG.md file
        List<Release> releaseList = parseChangeLogFile();
        generateReleaseNotes(packageName, releaseList);
    }
    //===============================================================
    //===============================================================
    private List<Release> parseChangeLogFile() throws PogoException {
        List<Release> releaseList = new ArrayList<>();
        List<String> lines = ParserTool.readFileLines(CHANGE_LOG_FILE, true);
        List<String> releaseLines = new ArrayList<>();
        for (String line : lines) {
            //noinspection StatementWithEmptyBody
            if (line.startsWith("# ")) {
                // forget page title
            }
            else
            if (line.startsWith("####")) {
                if (!releaseLines.isEmpty())
                    releaseList.add(new Release(releaseLines));
                releaseLines.clear();
                releaseLines.add(line.substring(4).trim());
            }
            else
                releaseLines.add(line);
        }
        if (!releaseLines.isEmpty())
            releaseList.add(new Release(releaseLines));
        return releaseList;
    }
    //===============================================================
    //===============================================================
    private String getPackageName() throws PogoException {
        //  Get a java source file in specified path
        //  And parse its package name
        File dir = new File(notesPath);
        if (dir.exists()) {
            String[] fileNames = dir.list();
            if (fileNames != null) {
                for (String fileName : fileNames) {
                    String absoluteName = notesPath + '/' + fileName;
                    File file = new File(absoluteName);
                    if (file.isFile() && fileName.endsWith(".java")) {
                        List<String> lines = ParserTool.readFileLines(absoluteName, false);
                        for (String line : lines) {
                            if (line.startsWith("package")) {
                                return line.substring("package".length(), line.length() - 1).trim();
                            }
                        }
                    }
                }
            }
            throw new PogoException("package cannot be found");
        }
        else
            throw new PogoException(notesPath + " path not found");
    }
    //===============================================================
    //===============================================================
    private void generateReleaseNotes(String packageName, List<Release> releaseList) throws PogoException {
        StringBuilder sb = new StringBuilder("\npackage " + packageName+";\n\n");
        sb.append(getComments());

        sb.append("public interface ").append(RELEASE_CLASS).append(" {\n");
        sb.append("\tString htmlString = \n");
        sb.append(getHtmlHeader());

        for (Release release : releaseList)
            sb.append(release.toHtml());

        sb.append(getHtmlFooter());
        sb.append("}");

        String fileName = notesPath+'/'+RELEASE_FILE;
        ParserTool.writeFile(fileName, sb.toString());
        System.out.println(fileName + "  has been generated.");
    }
    //===============================================================
    //===============================================================
    //===============================================================
    private String getHtmlHeader() {
        return  "\t\t\"<!DOCTYPE HTML PUBLIC \\\"-//W3C//DTD HTML 3.2//EN\\\">\\n\" + \n" +
                "\t\t\"<html>\\n\" + \n" +
                "\t\t\"<head>\\n\" + \n" +
                "\t\t\"<title> Release Notes </title>\\n\" + \n" +
                "\t\t\"</head>\\n\" + \n" +
                "\t\t\"<body text=\\\"#000000\\\" bgColor=\\\"#FFFFFF\\\" link=\\\"#0000FF\\\" vLink=\\\"#7F00FF\\\" aLink=\\\"#FF0000\\\">\\n\" + \n" +
                "\t\t\"<p><!-------TITLE------></p>\\n\" + \n" +
                "\t\t\"<center><h2> " + "Release Notes" + " </h2>\\n\" + \n"+
                "\t\t\"generated: " + getDate() + "</center>\\n\" + \n";
    }
    //===============================================================
    //===============================================================
    private String getHtmlFooter() {
        return "\t\t\"</body>\\n\" + \n\t\t\"</html>\\n\"\n\t;\n";
    }
    //===============================================================
    //===============================================================
    private String getComments() {
        return "/**\n * HTML code to display Release Notes for this package.\n"+
               " * It is generated by Pogo classes from a text file.\n */\n\n";
    }
    //===============================================================
    //===============================================================
    private static String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy  HH:mm");
        return simpleDateFormat.format(new Date());
    }
    //===============================================================
    //===============================================================

    //===============================================================
    //===============================================================
    public static void main(String[] args) {
        try {
            if (args.length==0) {
                throw new PogoException("target path to generate " + RELEASE_FILE + " ?");
            }
                new UpdateJavaReleaseNotes(args[0]);
        } catch (PogoException e) {
            System.err.println(e.toString());
        } catch (Exception e) {
            //System.err.println(e.toString());
            e.printStackTrace();
        }
    }
    //===============================================================
    //===============================================================





    //===============================================================
    //===============================================================
    private class Release {
        private String title;
        private List<String> changes = new ArrayList<>();
        //===========================================================
        private Release(List<String> lines) {
            title = lines.remove(0);
            changes.addAll(lines);
        }
        //===========================================================
        private String checkSyntax(String line) {
            int start = 0;
            int end;

            //  Replace double cotes
            StringBuilder sb = new StringBuilder();
            while ((end=line.indexOf('\"', start))>=0) {
                sb.append(line.substring(start, end)).append("\\\"");
                start = end+1;
            }
            sb.append(line.substring(start));
            line = sb.toString();

            //  For simple cotes
            start = 0;
            sb = new StringBuilder();
            while ((end=line.indexOf('\'', start))>=0) {
                sb.append(line.substring(start, end)).append("\\\'");
                start = end+1;
            }
            sb.append(line.substring(start));
            return sb.toString();
        }
        //===========================================================
        private String toHtml() {
            StringBuilder sb = new StringBuilder();
            sb.append("\t\t\"<li><b>").append(title).append("</b><br>\" + \n");
            for (String change : changes) {
                change = checkSyntax(change);
                sb.append("\t\t\"&nbsp; &nbsp; &nbsp; ").append(change).append("<br>\" + \n");
            }
            return sb.toString();
        }
        //===========================================================
        public String toString() {
            StringBuilder sb = new StringBuilder(title+'\n');
            for (String change : changes)
                sb .append('\t').append(change).append('\n');
            return sb.toString();
        }
        //===========================================================
    }
    //===============================================================
    //===============================================================
}
