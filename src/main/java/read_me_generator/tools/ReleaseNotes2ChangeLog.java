//+======================================================================
// :  $
//
// Project:   Tango
//
// Description:  java source code for Tango manager tool..
//
// : pascal_verdier $
//
// Copyright (C) :      2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,
//						European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
// This file is part of Tango.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// :  $
//
//-======================================================================

package read_me_generator.tools;

import org.tango.pogo.pogo_gui.tools.ParserTool;
import org.tango.pogo.pogo_gui.tools.PogoException;

import java.io.File;
import java.util.List;
import java.util.StringTokenizer;


/**
 * This class is able to convert ReleaseNote file to CHANGELOG.md
 *
 * @author verdier
 */

public class ReleaseNotes2ChangeLog {
    private static final String OUTPUT_FILE = "CHANGELOG.md";
    //===============================================================
    //===============================================================
    public ReleaseNotes2ChangeLog() throws PogoException  {
        String fileName = "ReleaseNote";
        if (!new File(fileName).exists()) {
            fileName += "s";
            if (!new File(fileName).exists()) {
                throw new PogoException("ReleaseNote file not found !");
            }
        }
        List<String> lines = ParserTool.readFileLines(fileName, false);
        String newCode = buildChangeLog(lines);
        ParserTool.writeFile(OUTPUT_FILE, newCode);
        System.out.println(OUTPUT_FILE + " has been generated");
    }
    //===============================================================
    //===============================================================
    private String buildChangeLog(List<String> lines) {
        StringBuilder sb = new StringBuilder("# Changelog\n");
        for (String line : lines) {
            if (!line.startsWith("#")) {
                if (line.endsWith(":")) {
                    sb.append("\n#### ").append(splitLine(line));
                }
                else {
                    sb.append("    ").append(line);
                }
                sb.append('\n');
            }
        }
        return sb.toString();
    }
    //===============================================================
    //===============================================================
    private String splitLine(String line) {
        StringTokenizer stk = new StringTokenizer(line);
        if (stk.countTokens()==2) {
            String release = stk.nextToken() + "  (";
            String date = stk.nextToken();
            if (date.endsWith(":")) {
                date = date.substring(0, date.length()-1) + "):";
            }
            else
                date += "):";
            return release + date;
        }
        return line;
    }
    //===============================================================
    //===============================================================

    //===============================================================
    //===============================================================
    public static void main(String[] args) {
        try {
            new ReleaseNotes2ChangeLog();
        } catch (PogoException e) {
            e.print_exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
