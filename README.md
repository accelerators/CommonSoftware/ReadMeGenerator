# Project ReadMeGenerator

Maven Java project

A tool to use scripts to generate README.md,.... files and try to fill it

## Cloning 

How to clone the peoject:

```
git clone  git@gitlab.esrf.fr:accelerators/CommonSoftware/ReadMeGenerator
```

## Building and Installation

See the [INSTALL.md](INSTALL.md)

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Pogo.jar

#### Toolchain Dependencies

* javac 7 or higher
* maven

### Build

Instructions on building the project.

```
cd ReadMeGenerator
mvn package
```
## License

The code is released under the **GPL3** licence
