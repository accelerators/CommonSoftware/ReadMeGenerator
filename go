
TANGO_HOME=/segfs/tango/
APPLI_HOME=$TANGO_HOME/release/java/appli
LIB_HOME=$TANGO_HOME/release/java/lib

JTANGO=$LIB_HOME/JTango.jar
POGO=$APPLI_HOME/Pogo.jar



PROJ_HOME=$TANGO_HOME/tools/admin/ReadMeGenerator
PROJ_JAR=
PROJ_CLASSES=$PROJ_HOME/target/classes
PROJ_PACKAGE=read_me_generator
PROJ_MAIN_CLASS=ReadMeGenerator


CLASSPATH=$PROJ_CLASSES:$POGO
export CLASSPATH
#echo $CLASSPATH


SCRIPT_HOME=/segfs/acu/HelperScripts
SCRIPT_NAME=$SCRIPT_HOME/give_me_project_docs.py
export SCRIPT_NAME

java $PROJ_PACKAGE.$PROJ_MAIN_CLASS
